# ubuntu_ansible_playbook

This playbook sets up an operational machine for a developer

# Users
- This playbook create a user /playbooks/roles/users/vars/main.yml
- After play this playbook, don't forget to update the user's password with ```sudo passwd {{user}}```

# packages
- This playbook will install :
    - apt-transport-https
    - libxslt-dev 
    - libxml2-dev 
    - libvirt-dev 
    - zlib1g-dev 
    - ruby-dev 
    - ruby-libvirt 
    - ebtables
    - code
    - git
    - brave-browser
    - dotnet-sdk-8.0
    - docker-ce
    - docker-ce-cli
    - containerd.io 
    - docker-compose-plugin
    - vagrant
    - qemu-kvm 
    - libvirt-daemon-system 
    - libvirt-clients 
    - bridge-utils 
    - virt-manager 
    - ksmtuned 
    - cockpit
    - cockpit-machines

# TODO
- browser bookmarks
- pinniped
- k8s
- Golang